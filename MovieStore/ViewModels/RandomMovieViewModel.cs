﻿using MovieStore.Models;
using System.Collections.Generic;

namespace MovieStore.ViewModels
{
    public class RandomMovieViewModel
    {
        public Movie Movie { get; set; }
        public List<Customer> Customers { get; set; }
    }
}