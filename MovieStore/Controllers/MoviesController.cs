﻿using MovieStore.Models;
using MovieStore.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;
using System;

namespace MovieStore.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies/Random
        public ActionResult Index()
        {
            var movies = GetMovies();

            return View(movies);
        }

        private IEnumerable<Movie> GetMovies()
        {
            return new List<Movie>
            {
                new Movie{ Id = 1, Name = "Wall-E"},
                new Movie{Id = 2, Name = "Up" }
             };
        }
    }
}