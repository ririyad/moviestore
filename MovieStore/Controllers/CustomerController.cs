﻿using MovieStore.Models;
using MovieStore.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;
using System;
using System.Linq;

namespace MovieStore.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer1
        public ViewResult Index()
        {
            var customers = GetCustomers();

            return View(customers);
        }

        public ActionResult Details(int id)
        {
            var customer = GetCustomers().SingleOrDefault(c => c.Id == id);
            if (customer == null)
                return HttpNotFound();
            return View(customer);
        }

        private IEnumerable<Customer> GetCustomers()
        {
            return new List<Customer>
            {
                new Customer{ Id = 1, Name = "John Smith" },
                new Customer{Id = 2, Name = "Marie Williams"}
            };
        }
    }
}